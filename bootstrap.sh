#! /usr/bin/env bash

#argunents

#synchronize package database
apt-get update

#update packages
apt-get --assume-yes upgrade

#add Erlang Solutions repo
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
dpkg -i erlang-solutions_1.0_all.deb
rm erlang-solutions_1.0_all.deb
apt-get update

#install elixir
apt-get --assume-yes install elixir

#install node.js(node is provited by nodejs-legacy in debian based distros)
apt-get --assume-yes install nodejs-legacy
#add switch for the alternative

#install datatabase
#default postgresql
apt-get --assume-yes install postgresql postgresql-contrib
echo "After login run sudo -u postgres psql postgres and \\password postgres to setup postgres user password(temporarly until automated)"
#or mysql
#apt-get install mysql-server



#install inotify-tools
apt-get --assume-yes install inotify-tools

#install hex
sudo -H -u vagrant mix local.hex --force

#intsall phoenix
sudo -H -u vagrant mix archive.install https://github.com/phoenixframework/phoenix/releases/download/v1.0.3/phoenix_new-1.0.3.ez --force
