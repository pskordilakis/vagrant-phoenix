Vagrant-Phoenix
===============

Vagrantfile and bootstrap script to set up a vagrant box for developement with
phoenix framework

Phoenix Framework
-----------------

Phoenix is a web framework writen in Elixir language running on ErlangVM

Prerequisites
-------------

- VirualBox
- Vagrant

Configuration
-------------
Shared folder by default is ~/project => /home/vagrant.
Change host folder to project path(Vagrantfile:40)

Usage
-----

```
#!shell
vagrant box add ubuntu/trusty64
mkdir project_name
cd project_name
git clone https://skordipan@bitbucket.org/skordipan/vagrant-phoenix.git
vagrant up
```
